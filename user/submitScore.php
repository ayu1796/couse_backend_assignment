<?php

SESSION_START();

include("../database.php"); // sertakan database.php untuk dapat menggunakan class database

$db = new Database(); // membuat objek baru dari class database agar dapat menggunakan fungsi didalamnya

$nik = (isset($_SESSION['nik'])) ? $_SESSION['nik'] : "";

$token = (isset($_SESSION['token'])) ? $_SESSION['token'] : "";

if($token && $nik)

{

   $result = $db->execute("SELECT * FROM user_tbl WHERE nik = '".$nik."' AND token = '".$token."' AND status = 1 ");

   if(!$result)

   {

       // redirect ke halaman login, data tidak valid

       header("Location: http://localhost/course_backend_assignment/");

   }

   // abaikan jika token valid

   $userdata = $db->get("SELECT user_tbl.nik as nik, user_tbl.nama_depan as nama_depan, user_tbl.nama_belakang as nama_belakang,

                       user_tbl.alamat as alamat, user_tbl.kode_pos as kode_pos, kota_tbl.nama_kota as nama_kota,

                       provinsi_tbl.nama_provinsi as nama_provinsi

                       from user_tbl,kota_tbl, provinsi_tbl WHERE user_tbl.nik = '".$nik."' AND

                       user_tbl.kota_id = kota_tbl.kota_id AND kota_tbl.provinsi_id = provinsi_tbl.provinsi_id");               

   $userdata = mysqli_fetch_assoc($userdata);  

}

else

{

   header("Location: http://localhost/course_backend_assignment/");

}

$notification = (isset($_SESSION['notification'])) ? $_SESSION['notification'] : "";

if($notification)

{

   echo $notification;

   unset($_SESSION['notification']);   

}

?>

PAGE : SUBMIT SCORE

<table border=1>

   <tr>

       <td>MENU</td>

       <td><a href="http://localhost/course_backend_assignment/user/">HOME</a></td>

       <td><a href="http://localhost/course_backend_assignment/user/statistik.php">STATISTIK</a></td>       

       <td><a href="http://localhost/course_backend_assignment/user/leaderboard.php">LEADERBOARD</a></td>

       <td><a href="http://localhost/course_backend_assignment/user/logout.php">LOGOUT</a></td>

   </tr>

</table>

<br>

<form action="submitScore_process.php" method="POST">

<table>

  <tr>

      <td>nik</td><td>:</td><td><input type="text" name="nik" required></td>

  </tr>
  <tr>
    <td>status</td><td>:</td>
    <td>
      <select name="status">
        <option value="true">true</option>
        <option value="false">false</option>
     </select>
   </td>
  </tr>
  <tr>
  <td>Pilih Game</td><td>:</td>
       <td><select name="game_id">

           <?php

           $gamedata = $db->get("SELECT game_id,nama FROM game_tbl WHERE status=1");                                

           while($row = mysqli_fetch_assoc($gamedata))

           {

               ?>

               <option value="<?php echo $row['game_id']?>"><?php echo $row['nama']?></option>

               <?php

           }

           ?>

       </select></td>
  </tr>
   <tr>

      <td>score</td><td>:</td><td><input type="text" name="score" required></td>

  </tr>
  <tr>
  <td>Level</td><td>:</td>
       <td><select name="level_id">

           <?php

           $leveldata = $db->get("SELECT level_id,level FROM level_tbl");                                

           while($row = mysqli_fetch_assoc($leveldata))

           {

               ?>

               <option value="<?php echo $row['level_id']?>"><?php echo $row['level']?></option>

               <?php

           }

           ?>

       </select></td>
  </tr>
   <tr>

      <td colspan=3><input type="submit" value="SUBMIT"></td>

  </tr> 
</table>

</form>

<button><a href="http://localhost/course_backend_assignment/user/leaderboard.php">Back to Leaderboard</button>