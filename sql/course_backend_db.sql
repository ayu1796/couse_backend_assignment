-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Bulan Mei 2020 pada 15.15
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course_backend_db`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET GAME DATA BY STATUS` (IN `pStatus` BOOLEAN)  NO SQL
BEGIN
	SELECT * FROM game_tbl WHERE status = pStatus;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `game_tbl`
--

CREATE TABLE `game_tbl` (
  `game_id` int(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tipe_leaderboard` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `game_tbl`
--

INSERT INTO `game_tbl` (`game_id`, `nama`, `tipe_leaderboard`, `status`) VALUES
(2, 'Game-001', 1, 1),
(3, 'Game-002', 1, 1),
(4, 'Game-003', 1, 1),
(5, 'Game-004', 1, 1),
(6, 'Game-005', 1, 1),
(7, 'Game-006', 1, 1),
(8, 'Game-007', 1, 1),
(9, 'Game-008', 1, 1),
(10, 'Game-009', 1, 0),
(11, 'Game-010', 1, 0),
(12, 'Game-011', 1, 0),
(13, 'Game-012', 1, 0),
(14, 'Game-014', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota_tbl`
--

CREATE TABLE `kota_tbl` (
  `kota_id` int(255) NOT NULL,
  `provinsi_id` int(255) NOT NULL,
  `nama_kota` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kota_tbl`
--

INSERT INTO `kota_tbl` (`kota_id`, `provinsi_id`, `nama_kota`, `status`) VALUES
(1, 1, 'Bandung', 1),
(2, 1, 'Sumedang', 1),
(3, 1, 'Garut', 1),
(4, 2, 'Semarang', 1),
(5, 2, 'Surakarta', 1),
(6, 2, 'Tegal', 1),
(7, 3, 'Surabaya', 1),
(8, 3, 'Malang', 1),
(9, 3, 'Batu', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level_tbl`
--

CREATE TABLE `level_tbl` (
  `level_id` int(50) NOT NULL,
  `level` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `level_tbl`
--

INSERT INTO `level_tbl` (`level_id`, `level`) VALUES
(3, 1),
(4, 2),
(5, 3),
(6, 4),
(7, 5),
(8, 6),
(9, 7),
(10, 8),
(11, 9),
(12, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode_tbl`
--

CREATE TABLE `periode_tbl` (
  `periode_id` int(255) NOT NULL,
  `periode_awal` time NOT NULL,
  `periode_akhir` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `periode_tbl`
--

INSERT INTO `periode_tbl` (`periode_id`, `periode_awal`, `periode_akhir`) VALUES
(1, '00:12:00', '00:15:00'),
(2, '00:08:00', '00:11:00'),
(3, '00:07:00', '00:08:00'),
(4, '00:17:00', '00:19:00'),
(5, '00:21:00', '00:21:30'),
(6, '00:11:00', '00:17:00'),
(7, '00:19:00', '00:20:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi_tbl`
--

CREATE TABLE `provinsi_tbl` (
  `provinsi_id` int(255) NOT NULL,
  `nama_provinsi` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `provinsi_tbl`
--

INSERT INTO `provinsi_tbl` (`provinsi_id`, `nama_provinsi`, `status`) VALUES
(1, 'Jawa Barat', 1),
(2, 'Jawa Tengah', 1),
(3, 'Jawa Timur', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_game_data_tbl`
--

CREATE TABLE `user_game_data_tbl` (
  `user_game_data_id` int(100) NOT NULL,
  `NIK` varchar(16) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `game_id` int(100) NOT NULL,
  `score` int(255) NOT NULL,
  `level_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_game_data_tbl`
--

INSERT INTO `user_game_data_tbl` (`user_game_data_id`, `NIK`, `status`, `game_id`, `score`, `level_id`) VALUES
(4, '1000000000000001', 1, 2, 67, 2),
(5, '1000000000000001', 1, 2, 60, 1),
(6, '1000000000000002', 1, 2, 87, 4),
(7, '1000000000000003', 1, 2, 61, 4),
(8, '1000000000000001', 1, 7, 80, 4),
(9, '1000000000000001', 1, 7, 67, 7),
(10, '1000000000000001', 1, 7, 97, 5),
(11, '1000000000000001', 1, 8, 60, 9),
(12, '1000000000000002', 1, 8, 70, 1),
(13, '1000000000000002', 1, 8, 99, 1),
(14, '1000000000000002', 1, 8, 70, 3),
(15, '1000000000000003', 1, 8, 50, 5),
(17, 'admin', 1, 2, 88, 6),
(18, 'admin', 1, 2, 78, 4),
(19, 'admin', 1, 2, 90, 1),
(20, 'admin', 1, 2, 38, 4),
(21, 'admin', 1, 2, 68, 5),
(22, 'admin', 1, 2, 99, 5),
(23, 'admin', 1, 7, 80, 5),
(24, 'admin', 1, 7, 70, 5),
(25, 'admin', 1, 7, 90, 6),
(26, 'admin', 1, 7, 58, 6),
(27, 'admin', 1, 7, 48, 6),
(28, 'admin', 1, 7, 100, 5),
(29, 'admin', 1, 8, 40, 5),
(30, 'admin', 1, 8, 60, 3),
(31, 'admin', 1, 8, 70, 3),
(32, 'admin', 1, 8, 98, 3),
(33, 'admin', 1, 8, 68, 3),
(34, 'admin', 1, 8, 50, 3),
(35, 'admin', 0, 3, 70, 1),
(36, 'admin', 0, 9, 70, 4),
(37, 'admin', 0, 3, 70, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_tbl`
--

CREATE TABLE `user_tbl` (
  `NIK` varchar(16) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `nomor_handphone` int(15) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `kota_id` int(255) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `password` varchar(200) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_tbl`
--

INSERT INTO `user_tbl` (`NIK`, `nama_depan`, `nama_belakang`, `nomor_handphone`, `tanggal_lahir`, `tempat_lahir`, `email`, `alamat`, `kota_id`, `kode_pos`, `status`, `password`, `token`) VALUES
('1000000000000001', 'Ani', 'Marni', 2147483647, '1990-01-01', 'Bandung', 'animarni@gmail.com', 'Gedebage, Bandung', 1, 0, 1, 'f43433f2d32d', '4f33gf43h45656'),
('1000000000000002', 'Budi', 'Yanto', 2147483647, '1991-02-02', 'Bandung', 'budiyanto@gmail.com', 'Gedebage, Bandung', 1, 0, 1, 'f43433f24545', '4f3fdfd3h45656'),
('1000000000000003', 'Charlie', 'Darwin', 2147483647, '1992-03-03', 'Bandung', 'charliedarwin@gmail.com', 'Gedebage, Bandung', 1, 0, 1, 'f43433f2bv5g', '56565f43h45656'),
('admin', 'admin', 'admin', 2147483647, '2000-06-19', 'Bandung', 'admin@agate.id', 'Summarecon Bandung gedebage', 1, 1000, 1, '21232f297a57a5a743894a0e4a801fc3', '40440caf7feda95bf28a242257049e0e');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `game_tbl`
--
ALTER TABLE `game_tbl`
  ADD PRIMARY KEY (`game_id`);

--
-- Indeks untuk tabel `kota_tbl`
--
ALTER TABLE `kota_tbl`
  ADD PRIMARY KEY (`kota_id`),
  ADD KEY `provinsi_id` (`provinsi_id`);

--
-- Indeks untuk tabel `level_tbl`
--
ALTER TABLE `level_tbl`
  ADD PRIMARY KEY (`level_id`);

--
-- Indeks untuk tabel `periode_tbl`
--
ALTER TABLE `periode_tbl`
  ADD PRIMARY KEY (`periode_id`);

--
-- Indeks untuk tabel `provinsi_tbl`
--
ALTER TABLE `provinsi_tbl`
  ADD PRIMARY KEY (`provinsi_id`);

--
-- Indeks untuk tabel `user_game_data_tbl`
--
ALTER TABLE `user_game_data_tbl`
  ADD PRIMARY KEY (`user_game_data_id`),
  ADD KEY `NIK` (`NIK`),
  ADD KEY `game_id` (`game_id`);

--
-- Indeks untuk tabel `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`NIK`),
  ADD KEY `kota_id` (`kota_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `game_tbl`
--
ALTER TABLE `game_tbl`
  MODIFY `game_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `kota_tbl`
--
ALTER TABLE `kota_tbl`
  MODIFY `kota_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `level_tbl`
--
ALTER TABLE `level_tbl`
  MODIFY `level_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `periode_tbl`
--
ALTER TABLE `periode_tbl`
  MODIFY `periode_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `provinsi_tbl`
--
ALTER TABLE `provinsi_tbl`
  MODIFY `provinsi_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user_game_data_tbl`
--
ALTER TABLE `user_game_data_tbl`
  MODIFY `user_game_data_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kota_tbl`
--
ALTER TABLE `kota_tbl`
  ADD CONSTRAINT `kota_tbl_ibfk_1` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi_tbl` (`provinsi_id`);

--
-- Ketidakleluasaan untuk tabel `user_game_data_tbl`
--
ALTER TABLE `user_game_data_tbl`
  ADD CONSTRAINT `user_game_data_tbl_ibfk_1` FOREIGN KEY (`NIK`) REFERENCES `user_tbl` (`NIK`),
  ADD CONSTRAINT `user_game_data_tbl_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game_tbl` (`game_id`);

--
-- Ketidakleluasaan untuk tabel `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD CONSTRAINT `user_tbl_ibfk_1` FOREIGN KEY (`kota_id`) REFERENCES `kota_tbl` (`kota_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
